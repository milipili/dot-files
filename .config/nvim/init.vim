set nocompatible
filetype off
set rtp+=~/.vim/

" install VimPlug
if empty(glob("~/.vim/autoload/plug.vim"))
	execute '!mkdir -p ~/.vim/plugged'
	execute '!mkdir -p ~/.vim/autoload'
	execute '!curl -fLo ~/.vim/autoload/plug.vim https://raw.github.com/junegunn/vim-plug/master/plug.vim'
endif

call plug#begin('~/.vim/plugged')

Plug 'airblade/vim-rooter'
Plug 'bogado/file-line', {'branch': 'main'}
Plug 'chrisbra/NrrwRgn'
Plug 'editorconfig/editorconfig-vim'
" Plug 'godlygeek/tabular'
Plug 'inkarkat/vim-ingo-library'
Plug 'inkarkat/vim-mark' " Highlight several words in different colors simultaneously
Plug 'itchyny/lightline.vim'
Plug 'joker1007/vim-markdown-quote-syntax'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'luochen1990/rainbow'
Plug 'mhinz/vim-grepper'
Plug 'mhinz/vim-startify'
Plug 'tpope/vim-fugitive'
Plug 'xolox/vim-misc' " used by vim-session
Plug 'xolox/vim-session'
Plug 'dominikduda/vim_current_word'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'lewis6991/gitsigns.nvim', {'branch': 'main'}
Plug 'nvim-tree/nvim-tree.lua'
Plug 'folke/trouble.nvim'

" TreeSitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Completion
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp', {'branch': 'main'}
Plug 'hrsh7th/cmp-buffer', {'branch': 'main'}
Plug 'hrsh7th/cmp-path', {'branch': 'main'}
Plug 'hrsh7th/cmp-cmdline', {'branch': 'main'}
Plug 'hrsh7th/nvim-cmp', {'branch': 'main'}
Plug 'p00f/clangd_extensions.nvim'

" Telescope
Plug 'nvim-tree/nvim-web-devicons'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim' ", { 'tag': '0.1.5' }


" Unused themes
" Plug 'ajmwagar/vim-deus'
" Plug 'altercation/vim-colors-solarized'
" Plug 'Blevs/vim-dzo'
" Plug 'cocopon/iceberg.vim'
" Plug 'exitface/synthwave.vim'
" Plug 'Marfisc/vorange'
" Plug 'morhetz/gruvbox'
" Plug 'nanotech/jellybeans.vim'
" Plug 'rakr/vim-one'
" Plug 'tyrannicaltoucan/vim-deep-space'
" Plug 'w0ng/vim-hybrid'
" Plug 'whatyouhide/vim-gotham'
" Plug 'arcticicestudio/nord-vim', {'branch': 'main'}
" Plug 'rafamadriz/neon'

" Themes
Plug 'folke/tokyonight.nvim', {'branch': 'main'}
Plug 'catppuccin/nvim'
Plug 'projekt0n/github-nvim-theme'

" Apply
filetype plugin indent on
call plug#end()


"
" ---- plugins options ---
"
let g:rooter_patterns = ['.git', 'build']

let g:vim_current_word#highlight_delay = 300 "ms

" nvim-tree-lua
nmap <F7> :NvimTreeToggle<CR>

" ligtline
let g:lightline = {
	\ 'colorscheme': 'one',
	\ 'component': {
	\   'readonly': '%{&readonly?"x":""}',
	\ },
	\ 'separator': { 'left': '', 'right': '' },
	\ 'subseparator': { 'left': '|', 'right': '|' },
	\ 'active': {
		\ 'left': [['statuslinetabs', 'lineinfo'], ['gitbranch', 'readonly', 'modified']],
		\ 'right': [['mode'], ['paste'], ['relativepath']],
	\ },
	\ 'component_function': {
		\ 'gitbranch': 'FugitiveHead'
	\ }
\ }

" Narrow Region - press 'n' after selecting some text to edit only this part
vmap n :NarrowRegion<CR>

" vim-session
let g:session_autoload = 'no'
let g:session_autosave = 'no'

" rainbow
let g:rainbow_active = 0

" Telescope
nnoremap <leader>e :Telescope find_files<cr>
nnoremap <leader>g :Telescope live_grep<cr>

" goyo
let g:goyo_width = '140'
let g:goyo_height = '100%'
nnoremap <C-f> :Goyo<cr>


"
" ---- misc options ----
"

set encoding=utf8
scriptencoding utf-8

set whichwrap=b,s,l,h,<,>,[,]
set icon
set title
set showbreak=\\
set noeol
set et
set hlsearch
set keywordprg=man\ -k
set formatprg=fmt
set novisualbell
set ttyfast
set hidden
set switchbuf=useopen
set showmatch
set matchtime=1
set showcmd
set lazyredraw
set viminfo+=!
set history=40
set smartcase
set nofoldenable
set incsearch
" set ignorecase
set splitbelow " open splits below and right side by default
set splitright
set cursorline
set nolist

" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowritebackup
set nowb
set noswapfile

" tabs war
set autoindent
set tabstop=4
set noexpandtab
set shiftwidth=4
set softtabstop=4
set cinoptions=Ls,l1,g0
"
" Help NeoVim check for modified files: https://github.com/neovim/neovim/issues/2127
autocmd BufEnter,FocusGained * checktime

"
" ---- shortcuts ----
"

" Hide coloration of found words
map <C-C> :nohlsearch<CR>

" Paste
map  <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>

" <tab> / <shift-tab> to indent and unindent
nnoremap <Tab> >>_
nnoremap <S-Tab> <<_
inoremap <S-Tab> <C-D>
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv


"
" ---- gui ----
"

" before the first colorscheme command will ensure that the highlight group
" gets created and is not cleared by future colorscheme commands
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
highlight ExtraWhitespace ctermbg=yellow guibg=yellow

highlight CursorLine   guibg=#191919 gui=none
highlight CursorColumn guibg=#181818
highlight Visual       guifg=White guibg=red gui=none
highlight StatusLine   cterm=none ctermfg=white ctermbg=darkblue gui=none guifg=white guibg=darkblue

" colorscheme tokyonight-storm
colorscheme catppuccin-mocha
colorscheme github_dark

highlight IncSearch ctermbg=darkblue ctermfg=cyan cterm=none

" Current Word
hi CurrentWord ctermbg=178 guibg=#d7af00 guifg=#121212
hi CurrentWordTwins ctermbg=237 guibg=#bcbcbc guifg=#121212

:lua require('init')
