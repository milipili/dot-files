require("ibl").setup{ -- lukas-reineke/indent-blankline.nvim
	scope = { enabled = false },
}
require('lsp')
require('gitsigns').setup()

require('telescope').setup({
	defaults = {
		file_ignore_patterns = {
			"node_modules/.*",
			"%.env",
			"yarn.lock",
			"package-lock.json",
			"lazy-lock.json",
			"init.sql",
			"target/.*",
			".git/.*",
			"CMakeFiles",
			"%.a",
			"%.so",
			"_deps",
        },
    },
})
