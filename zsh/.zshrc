# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
# sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="obraun"
ZSH_COLORIZE_STYLE=solarized-dark

plugins=(gitfast colorize docker)

source $ZSH/oh-my-zsh.sh

alias v="nvim"
alias g="git"
alias l='ls -lAFh'

case "$OSTYPE" in
  darwin*)
	export PATH="/usr/local/sbin:$PATH" # homebrew
	# make
	nbcores=$(sysctl -n hw.ncpu)
	alias m="nice make -j $nbcores"
  ;;
  linux*)
    alias pbcopy='xclip -selection clipboard'
	# make
	nbcores=$(nproc --ignore=1)
	alias m="nice ionice make -j $nbcores"
  ;;
  dragonfly*|freebsd*|netbsd*|openbsd*)
  ;;
esac

if [ -d "$HOME/bin" ]; then
	export PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ]; then
	export PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "/usr/lib/ccache" ]; then
	export PATH="/usr/lib/ccache:$PATH"
fi
if [ -d "${HOME}/projects/vcpkg" ]; then
	export VCPKG_ROOT="${HOME}/projects/vcpkg"
fi

export EDITOR=nvim

export HISTSIZE=50000
setopt hist_ignore_all_dups
setopt hist_ignore_space

tabs 4
